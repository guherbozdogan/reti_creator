package sim

trait StepMethod {
  def execute(executor:Executor, args:Any):Any
}
trait Step {
  def execute(executor:Executor,args:Any):Any
}
case class Model(steps:List[Step]) {
  
 }

case class Stage(model:Model) {
  
  def execute(e:Executor,args:Any):Any = {
     e.initContext
     var result = model.steps.foldLeft(args)((a:Any, b:Step)=>{
       b.execute(e, a)
     })
     e.exitContext
     result
    
  }
 
}

trait Executor {
  def initContext:Unit =  {
    
  }
  def context:Any  
  
  def exitContext:Unit ={
    
  }
}


case object DefaultExecutor extends Executor {
  
  def context:Any  = null
}

case class ExecutorWithContext(val context:Any) extends Executor {
  
}