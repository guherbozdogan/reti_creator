package sim.spark
import org.apache.spark.sql.SparkSession
import sim.{Step, StepMethod, Executor,ExecutorWithContext}

case class SparkStep(val method:StepMethod) extends Step {
  
  def execute(executor:Executor,args:Any): Any = {
    method.execute(executor,args)
    }
}

case class SparkStepWithSelfContainedContext(val method:StepMethod) extends Step {
  
  val MAX_MEMORY="4g"
  def execute(executor:Executor, args:Any): Any = {
    val spark = SparkSession.builder.appName("Simple Application").master("local[*]")
     .config("spark.executor.memory", MAX_MEMORY)
    .config("spark.driver.memory", MAX_MEMORY)
    .getOrCreate()
    var result= method.execute(ExecutorWithContext(spark.asInstanceOf[Any]),args)
    spark.stop()
    result
    }
}

case class SparkExecutorWithContext() extends Executor {
  
    var sparkSession: SparkSession= null
     override def context:Any = {
      sparkSession.asInstanceOf[Any]
    }
    override def initContext:Unit = {
      sparkSession = SparkSession.builder.appName("Simple Application").master("local[2]").getOrCreate()
     }
    
 override def exitContext:Unit ={
    sparkSession.stop
  }
}