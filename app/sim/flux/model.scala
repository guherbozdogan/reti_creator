package sim.flux

import sim.{ Step, StepMethod, Executor, ExecutorWithContext }
import sim.spark.{ SparkStep, SparkExecutorWithContext }
import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.non_random.{ SphereCoordinateSample, SphereSampler }
import models.atmosphere.earth.{ EarthAtmosphere }
import models.atmosphere.{ Atmosphere, AtmosphereLayer, AtmosphereContent }
import org.apache.spark.sql.{ SparkSession, SQLContext }
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import utilities.shape.{ Sphere, SingleEndedLine }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import sim.{ StepMethod, Step, Model, Stage, Executor }
import scala.language.postfixOps
import models.light.spectral.SpectralPowerDistribution
import breeze.linalg._

case class FluxExtinctionModelParams(
  atmosphere:    Atmosphere,
  planet:        Planet,
  sun_light_dir: Vector3)

object FluxExtinctionModel {

  def main(args: Array[String]): Unit = {
    var planet = Earth()
    var atmosphere = EarthAtmosphere()
    atmosphere.build
    var sun_light_dir = Vector3(1, 0, 0)
    execute(FluxExtinctionModelParams(atmosphere, planet, sun_light_dir))
  }

  def execute(params: FluxExtinctionModelParams): Any = {
    var planet = params.planet
    var atmosphere = params.atmosphere
    var sun_light_dir = params.sun_light_dir
     
    var range =380 to 780 by 5 toArray
    
    var range_dist= SpectralPowerDistribution.planck(
        new DenseVector[Double](range.map(
                (a:Int)=>a.toDouble)),5778.0).toArray.toList
    
    
    var wavelengths = (range zip range_dist).map((a: Tuple2[Int,Double]) => Flux_of_Wavelength(WaveLength(a._1.toDouble), 80000.0*a._2)) .toList

    var sphere_sampler = SphereSampler(atmosphere.layers(
      atmosphere.layers.length - 1).radius)

    var samples = sphere_sampler.generate_samples.toSeq.zipWithIndex

    var initialFluxExtinctionStepMethodArg =
      FluxExtinctionStepMethodArg(
        samples.map(p => {
          Light(p._2, LightIntersection(
            atmosphere.layers.length - 1,
            p._1.entity, wavelengths) :: Nil)
        }),
        sun_light_dir,
        atmosphere,  planet)
    var steps = List.fill(atmosphere.layers.length * 2 - 1)(SparkStep(FluxExtinctionStepMethod()))
    //  var steps = List.fill()  (SparkStep(FluxExtinctionStepMethod()))

    var model = Model(steps)
    var stage = Stage(model)
    var executor = SparkExecutorWithContext()
    var tmp_light_trace = stage.execute(executor, initialFluxExtinctionStepMethodArg).asInstanceOf[FluxExtinctionStepMethodArg]
    
    //print(tmp_light_trace)

    var next_step_input = tmp_light_trace.light_trace
    var next_steps = List.fill(1)(SparkStep(FluxLayersSaveStepMethod()))
    var next_model = Model(next_steps)
    var next_stage = Stage(next_model)
    next_stage.execute(
      executor,
      FluxLayersSaveStepMethodArg(next_step_input, "data_out"))

  }

}
