package sim.flux
import models.light.{WaveLength, Color}
import models.light.color.RGBColor

trait FluxSection {
  def wave_set:List[WaveLength]  
  def lumen:Double
  
//  def color: RGBColor = {
//    RGBColor.build(wave_set)
//  }
  
}
