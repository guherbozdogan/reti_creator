package sim.flux

import sim.{ Step, StepMethod, Executor, ExecutorWithContext }
import sim.spark.{ SparkStep, SparkExecutorWithContext }
import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.non_random.{ SphereCoordinateSample, SphereSampler }
import models.atmosphere.earth.{ EarthAtmosphere }
import models.atmosphere.{ Atmosphere, AtmosphereLayer, AtmosphereContent }
import org.apache.spark.sql.{ SparkSession, SQLContext }
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import utilities.shape.{ Sphere, SingleEndedLine }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import sim.{StepMethod, Step, Model, Stage, Executor}
import scala.language.postfixOps
//import sim.flux.{Light, LightIntersection}
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}
import models.light.color.RGBColor
import models.light.cie.{CIE,CIE_SRGB}
import breeze.linalg._
import models.light.Flux
case class FluxLayersSaveStepMethodArg(
  var light_trace:  Seq[Light],
  var file_path: String
  )

case class FluxLayersSaveStepMethod() extends StepMethod {

  override def execute(executor: Executor, args: Any): Any = {
    var tmpArgs = args.asInstanceOf[FluxLayersSaveStepMethodArg]
    var spark = executor.context.asInstanceOf[SparkSession]
    var context = spark.sparkContext
    var rdd = context.parallelize(tmpArgs.light_trace)
    
    var tmp = rdd.flatMap((light:Light) => {
      light.list.toSeq
    }).map((point:LightIntersection)=>{
      var color =  CIE_SRGB.cie.spec_to_rgb(DenseVector[Double](Flux.normalize_SPD(point.wavelengths))
          , null).asInstanceOf[RGBColor]
      
        ( point.atmosphere_layer, point.point.x,point.point.y, 
            point.point.z,
           color.R,  color.G, color.B)
    })
      
    //def spec_to_rgb(spec:DenseVector[Double], out_fmt:String=null):Any = {
      
     
    
//      (point.atmosphere_layer, ((point, RGBColor.build (point.wavelengths)) ::Nil))
//    }).reduceByKey((l1:List[Tuple2[LightIntersection,RGBColor]], l2:List[Tuple2[LightIntersection,RGBColor]]) =>{
//      l1++l2
//    }).map( (a:Tuple2[Int, List[Tuple2[LightIntersection,RGBColor]]]) => {
//      a._2
////      (a._2(0).atmosphere_layer, a._2(0).point.x,a._2(0).point.y, a._2(0).point.z,
////           color.R,  color.G, color.B)
//    }) 
    
   var tmp_df=  spark.createDataFrame(tmp).toDF("atmosphere_layer", "x", "y", "z", "R","G","B")
       tmp_df.repartition(1).write.
        format("com.databricks.spark.csv").
        option("header", "true").
        option("delimiter", ",").
        save(tmpArgs.file_path)
  }
}

