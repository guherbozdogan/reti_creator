package sim.flux

import sim.{ Step, StepMethod, Executor, ExecutorWithContext }
import sim.spark.{ SparkStep, SparkExecutorWithContext }
import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.non_random.{ SphereCoordinateSample, SphereSampler }
import models.atmosphere.earth.{ EarthAtmosphere }
import models.atmosphere.{ Atmosphere, AtmosphereLayer, AtmosphereContent }
import org.apache.spark.sql.{ SparkSession, SQLContext }
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import utilities.shape.{ Sphere, SingleEndedLine }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import sim.{ StepMethod, Step, Model, Stage, Executor }
import scala.language.postfixOps
//import sim.flux.{Light, LightIntersection}

case class FluxExtinctionStepMethodArg(
  var light_trace:   Seq[Light],
  var sun_light_dir: Vector3,
  var atmosphere:    Atmosphere, var planet: Planet)

case class FluxExtinctionStepMethod() extends StepMethod {
  override def execute(executor: Executor, args: Any): Any = {
    var tmpArgs = args.asInstanceOf[FluxExtinctionStepMethodArg]
    var context = executor.context.asInstanceOf[SparkSession].sparkContext
    var rdd = context.parallelize(tmpArgs.light_trace)
    var check_if_obscured_by_planet = (c: Vector3,
      sun_light: Vector3, planet: Planet) => {
      var rev_light = SingleEndedLine(
        Vector4(c.x, c.y, c.z, 1),
        Vector4(sun_light.x * (-1), sun_light.y * (-1), sun_light.z * (-1), 1))

      var sphere = Sphere(planet.equatorial_radius)
      if (sphere.intersect(rev_light).length > 0) {
        true
      } else {
        false
      }
    }

    var calculate_extinction = (light: Light,
      sun_light: Vector3,
      atmopshere: Atmosphere,
      planet: Planet) => {

      var cur_intersection = light.list(light.list.length - 1)
      var direction =  (0-1)
      if (light.list.length > 1) {
      
        if (cur_intersection.atmosphere_layer == 0) {
          if (light.list(light.list.length - 2).atmosphere_layer == 0) {
            direction = 1
          } else {
            direction = 0
          }
        } else {
          if ((cur_intersection.atmosphere_layer - light.list(light.list.length - 2).atmosphere_layer) < 0) {
            direction =  (0-1)
          } else {
            direction = 1
          }

        }

      } else {
        direction = (0-1)
      }


      if (direction == 1 && cur_intersection.atmosphere_layer == (atmopshere.layers.length - 1)) {
        light
      } else {
        var nextLayer = atmopshere.layers(cur_intersection.atmosphere_layer + direction)
        var nextLayersSphere = Sphere(nextLayer.radius)

        var light_vec = SingleEndedLine(
          Vector4(
            (cur_intersection.point.x + sun_light.x * 0.01).toFloat,
            (cur_intersection.point.y + sun_light.y * 0.01).toFloat,
            (cur_intersection.point.z + sun_light.z * 0.01).toFloat, 1),
          Vector4(sun_light.x, sun_light.y, sun_light.z, 1))
        var intersection = nextLayersSphere.intersect(light_vec)
        if (intersection.length < 1 ){ //&& (cur_intersection.atmosphere_layer != (atmopshere.layers.length-1))) {
          direction = 0

          nextLayer = atmopshere.layers(cur_intersection.atmosphere_layer + direction)
          nextLayersSphere = Sphere(nextLayer.radius)
          intersection = nextLayersSphere.intersect(light_vec)

        }
        intersection match {
          case Nil =>{ 
            
            if (cur_intersection.atmosphere_layer == (atmopshere.layers.length-1)){
              null
            }else{
              light
            }
          }
          case a :: b => {

            var light_line = a - cur_intersection.point
            var path_length = math.floor(light_line.magnitude)
            //var path_length = light_line.magnitude
            if (check_if_obscured_by_planet(a, sun_light, planet)) {

              Light(light.id, light.list ++ (LightIntersection(cur_intersection.atmosphere_layer + direction, a,
                cur_intersection.wavelengths.map((l: Flux_of_Wavelength) => {
                  Flux_of_Wavelength(l.wavelength, 0.0)
                })) :: Nil))
            } else {
              var atmosphere_contents = atmopshere.layers(cur_intersection.atmosphere_layer).contents
              Light(light.id, light.list ++ (LightIntersection(cur_intersection.atmosphere_layer + direction, a,
                cur_intersection.wavelengths.map((l: Flux_of_Wavelength) => {
                  atmosphere_contents.foldLeft(l)(
                    (v: Flux_of_Wavelength, content: AtmosphereContent) => {
                      //println(content.extinction_multiplier(l.wavelength.v, path_length))
                      Flux_of_Wavelength(v.wavelength, v.intensity * content.extinction_multiplier(l.wavelength.v, path_length))
                    })
                })) :: Nil))
            }
          }
        }
      }
    }

    FluxExtinctionStepMethodArg(
      rdd.map(light => {
        calculate_extinction(
          light,
          tmpArgs.sun_light_dir,
          tmpArgs.atmosphere,
          tmpArgs.planet)
      }).collect().filter(_ != null),
      tmpArgs.sun_light_dir,
      tmpArgs.atmosphere,
      tmpArgs.planet).asInstanceOf[Any]

  }
}

