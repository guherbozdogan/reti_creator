package sim.flux

import sim.spark.{ SparkStep, SparkExecutorWithContext }
import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.non_random.{ SphereCoordinateSample, SphereSampler }
import models.atmosphere.earth.{ EarthAtmosphere }
import models.atmosphere.{ Atmosphere, AtmosphereLayer, AtmosphereContent }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import scala.language.postfixOps

case class Light(val id:Int, val list: List[LightIntersection])
case class LightIntersection(atmosphere_layer:Int, point: Vector3, wavelengths: List[Flux_of_Wavelength])
