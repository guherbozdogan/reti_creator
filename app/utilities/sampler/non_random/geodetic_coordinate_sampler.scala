package utilities.sampler.non_random
import utilities.coordinate_systems.geographic.CoordinateSystem

import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.Sample
import utilities.shape.{ SingleEndedLine, Geoid }
import utilities.coordinate_systems.geographic.geocentric.spherical.wgs84.WGS84
import utilities.coordinate_systems.geographic.geocentric.spherical.GeodBasedCoordinateSystem
import utilities.common.precision.PrecisionHelper
import scala.collection.mutable.ListBuffer
case class GeodeticCoordinateSample(val entity: Vector3, val latitude: Double = 0.0, val longitude: Double = 0.0, val index:Int=0) extends Sample[Vector3]

case class LongitudeContainer(val latitude: Double, 
    var longitude_precision: Int,val angle_increment: Double, var longitude_map: Map[String, GeodeticCoordinateSample] = Map(), var longitude_list:ListBuffer[GeodeticCoordinateSample] = ListBuffer[GeodeticCoordinateSample]()) {
  
  def get_coord(longt: Double):Option[GeodeticCoordinateSample]  = {
    var rem = longt % angle_increment 
    if(rem > 0.5*angle_increment){
      var nearest = (longt - rem) + angle_increment
      longitude_map.get(PrecisionHelper.format(nearest, longitude_precision))
    }else{
        var nearest = (longt - rem) 
      longitude_map.get(PrecisionHelper.format(nearest, longitude_precision))
    }
  }
  
}
case class LatitudeContainer(val latitude_precision: Int, val angle_increment: Double, 
    var latitude_map: Map[String, LongitudeContainer] = Map(), var latitude_list:ListBuffer[LongitudeContainer]=ListBuffer[LongitudeContainer]()) {
  
  def get_longitude_container(lat:Double): Option[LongitudeContainer] ={
    var rem = lat % angle_increment 
    if(rem > 0.5*angle_increment){
      var nearest = (lat - rem) + angle_increment
      latitude_map.get(PrecisionHelper.format(nearest, latitude_precision))
    }else{
        var nearest = (lat - rem) 
      latitude_map.get(PrecisionHelper.format(nearest, latitude_precision))
    }
  }
  def get_coord(lat:Double, longt: Double):Option[GeodeticCoordinateSample]  = {
    
      get_longitude_container(lat) match {
         case Some(longt_container) => {
           longt_container.get_coord(longt)
           
         }
         case None => None
      }
    
  }
}  


case class GeodeticCoordinateSampler(val coordinate_system: GeodBasedCoordinateSystem) {



  var acc:Int =0
  val unit_sample_count = 10.0 //on latitude
  val zenith_delta = 360 / unit_sample_count //in degrees
  var geoid: Geoid = Geoid(Vector3(
    coordinate_system.semi_major_axis.toFloat,
    coordinate_system.semi_major_axis.toFloat,
    coordinate_system.semi_minor_axis.toFloat))
  def convert_coordinate_to_geodetic(v: Vector3): Vector3 = {
    geoid.intersect(SingleEndedLine(Vector4(0.0f, 0.0f, 0.0f, 0.0f), Vector4(v.x, v.y, v.z, 0.0f))) match {
      case Nil    => null
      case a :: _ => a
    }
  }
  def gen_sample(zenith: Double, y_axis: Vector3): List[GeodeticCoordinateSample] = {
    val unit_count = (math.sin(math.toRadians(90 - zenith)) * unit_sample_count).toInt

    val angle_delta = 360.0 / unit_count
    val rotated_line: Vector3 = convert_coordinate_to_geodetic(Quaternion.forAxisAngle(
      y_axis,
      math.toRadians(zenith).toFloat) * Vector3(1.0f, 0.0f, 0.0f))
    List.range(0, unit_count) map ((a: Int) => {
      GeodeticCoordinateSample(Quaternion.forAxisAngle(Vector3(0.0f, 0.0f, 1.0f), math.toRadians(a * angle_delta).toFloat) * rotated_line)
    })

  }
  def generate_samples: List[GeodeticCoordinateSample] = {

    List[GeodeticCoordinateSample](GeodeticCoordinateSample(Vector3(0.0f, 0.0f, coordinate_system.semi_minor_axis.toFloat))) ++
      List.range(1, (unit_sample_count / 4).toInt).map((a: Int) => { a * zenith_delta }).foldLeft(List[GeodeticCoordinateSample]()) {
      (l: List[GeodeticCoordinateSample], a: Double) => { l ++ gen_sample( a, Vector3(0.0f, -1.0f, 0.0f)) } } ++
      gen_sample(0.0, Vector3(0.0f, -1.0f, 0.0f)) ++
      List.range(1, (unit_sample_count / 4).toInt).map((a: Int) => { a * zenith_delta }).foldLeft(List[GeodeticCoordinateSample]()) { (l: List[GeodeticCoordinateSample], a: Double) => {
        l ++ gen_sample( a, Vector3(0.0f, 1.0f, 0.0f)) } } ++
      List[GeodeticCoordinateSample](GeodeticCoordinateSample(Vector3(0.0f, 0.0f, -coordinate_system.semi_minor_axis.toFloat)))

  }

  def gen_longitudes(zenith: Double, y_axis: Vector3, is_north: Boolean = true): LongitudeContainer = {
    val unit_count = (math.sin(math.toRadians(90 - zenith)) * unit_sample_count).toInt

    val angle_delta = 360.0 / unit_count
    val prec = PrecisionHelper.calculate_precision(angle_delta)

    val rotated_line: Vector3 = convert_coordinate_to_geodetic(Quaternion.forAxisAngle(
      y_axis,
      math.toRadians(zenith).toFloat) * Vector3(1.0f, 0.0f, 0.0f))
    var lat = zenith
    if (is_north == false) {
      lat = 0 - zenith
    }
    var longt_container = LongitudeContainer(lat, prec,angle_delta, Map[String, GeodeticCoordinateSample]())

    List.range(0, unit_count) map ((a: Int) => {
      var angle = a * angle_delta
      var coord = GeodeticCoordinateSample(Quaternion.forAxisAngle(Vector3(0.0f, 0.0f, 1.0f), math.toRadians(angle).toFloat) * rotated_line, lat, 
          angle,acc)
       acc+=1
      longt_container.longitude_map += (PrecisionHelper.format(angle, prec) -> coord)
      longt_container.longitude_list += coord
    })
    longt_container
  }

  def generate_latitudes: LatitudeContainer = {

     val prec = PrecisionHelper.calculate_precision(zenith_delta)
      var lat_container = LatitudeContainer(prec, zenith_delta, Map[String, LongitudeContainer]())
      var zero_coord  =GeodeticCoordinateSample(Vector3(0.0f, 0.0f, coordinate_system.semi_minor_axis.toFloat), 90.0, 0.0,acc)
      var longt_container = LongitudeContainer(90.0, 1, 360.0,
          Map[String, GeodeticCoordinateSample]
        ( "0.0" -> zero_coord), ListBuffer[GeodeticCoordinateSample](zero_coord))
      lat_container.latitude_map +=  (PrecisionHelper.format(90.0, prec) -> longt_container )
      lat_container.latitude_list += longt_container 
        acc+=1
        
       List.range(1, (unit_sample_count / 4).toInt).map((a: Int) => { 
         
         var angle = a * zenith_delta 
         var longitude =gen_longitudes(angle , Vector3(0.0f, -1.0f, 0.0f), true)
         lat_container.latitude_map +=  (PrecisionHelper.format(angle, prec) ->  longitude)
         lat_container.latitude_list += longitude
         
       })
       var  equiator = gen_longitudes(0 , Vector3(0.0f, -1.0f, 0.0f), true)
       lat_container.latitude_map += (PrecisionHelper.format(0, prec) -> equiator )
       lat_container.latitude_list += equiator
       
//       
//       
//         List.range(1, (unit_sample_count / 4).toInt).map((a: Int) => { 
//         var angle = a * zenith_delta 
//       lat_container.latitude_map +=  (PrecisionHelper.format(-1*angle, prec) ->  gen_longitudes(angle , Vector3(0.0f, 1.0f, 0.0f), false))
//       })
//        lat_container.latitude_map +=  (PrecisionHelper.format(-90.0, prec) -> LongitudeContainer(-90.0, 1,360.0, Map[String, GeodeticCoordinateSample]
//        ( "0.0" -> GeodeticCoordinateSample(Vector3(0.0f, 0.0f, -coordinate_system.semi_minor_axis.toFloat), -90.0, 0.0))))
//       
       lat_container
         
  }

}