  package utilities.sampler.non_random
  import utilities.coordinate_systems.geographic.CoordinateSystem
  
  import com.github.jpbetz.subspace.{Quaternion, Vector4, Vector3}
  import utilities.sampler.Sample
  import utilities.shape.{SingleEndedLine, Sphere}
  
  case class SphereCoordinateSample(val entity:Vector3) extends Sample[Vector3]
   
    
  case class SphereSampler(val r: Double) {
   
    
    val unit_sample_count=100.0 //on latitude
    val zenith_delta = 360/unit_sample_count  //in degrees
    
    var sphere:Sphere=Sphere(r)
    def convert_coordinate_to_spherical(v:Vector3): Vector3 = {
      
      sphere.intersect(SingleEndedLine(Vector4(0.0f,0.0f,0.0f,0.0f),Vector4(v.x,v.y,v.z,0.0f))) match {
        
        case Nil  => null
        case a::_ => a
        
      }
        
    }
    def gen_sample(zenith:Double, y_axis:Vector3):List[SphereCoordinateSample]  = {
      val unit_count = (math.sin(math.toRadians(90-zenith))*unit_sample_count).toInt
      
      val angle_delta = 360.0/unit_count
      val rotated_line: Vector3  = convert_coordinate_to_spherical(Quaternion.forAxisAngle(y_axis, 
          math.toRadians(zenith).toFloat)*  Vector3(1.0f,0.0f, 0.0f))
      List.range(0, unit_count) map ( (a:Int)=>{
         SphereCoordinateSample(Quaternion.forAxisAngle(Vector3(0.0f,0.0f, 1.0f),  math.toRadians(a*angle_delta).toFloat ) * rotated_line)   
         } ) 
       
    }
    def generate_samples: List[SphereCoordinateSample] = {
    
      List[SphereCoordinateSample](SphereCoordinateSample(Vector3(0.0f,0.0f, r.toFloat))) ++ 
      List.range(1 , (unit_sample_count/4).toInt).map( (a:Int) => {
        a *zenith_delta} ).foldLeft(List[SphereCoordinateSample]()){ (l:List[SphereCoordinateSample], a:Double) => {l ++ gen_sample(a, Vector3(0.0f, -1.0f, 0.0f))} } ++  
          gen_sample(0.0, Vector3(0.0f,-1.0f,0.0f)) ++
         List.range(1 , (unit_sample_count/4).toInt).map( (a:Int) => {a *zenith_delta} ).foldLeft(List[SphereCoordinateSample]()){ (l:List[SphereCoordinateSample], a:Double) => {l ++ gen_sample(a, Vector3(0.0f, 1.0f, 0.0f))} } ++ 
         List[SphereCoordinateSample](SphereCoordinateSample(Vector3(0.0f,0.0f, -r.toFloat)))
          
    }
    
    
    
  }