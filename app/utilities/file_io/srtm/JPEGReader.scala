package file_io.srtm
import java.io.File
import javax.imageio.ImageIO
import java.awt.image.BufferedImage

class JPEGReader {
  
  
   //val photo1 = ImageIO.read(new File("./data/models/planet/earth/srtm/Srtm_ramp2.world.21600x10800.jpg"))

   var EverestHeight:Double = 8850.0 
   var img:java.awt.image.BufferedImage = null
   var width:Int = 0
   var height:Int =0 
   def read(filePath:String) :BufferedImage = {
     
    img = ImageIO.read(new File("photo.jpg"))

    //img1.getWidth
     width = img.getWidth
    height = img.getHeight
    
    img
   }
   
   def cleanImg:Unit  =  {
     img=null
     width=0
     height=0
   }
   
   
   def get_height(x:Int, y:Int):Double ={
     var col =  (img.getRGB(0,0)) & 0xff
     (col.toDouble / 0xff.toDouble )* EverestHeight 
   }
   val sea_color = 0x0c0c0c
   val sea_height = (0x0c.toDouble / 0xff.toDouble)*EverestHeight
   
   
}