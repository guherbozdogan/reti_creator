package utilities.gen.planet

import utilities.coordinate_systems.geographic.CoordinateSystem

import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.Sample
import utilities.shape.{ SingleEndedLine, Geoid }
import utilities.coordinate_systems.geographic.geocentric.spherical.wgs84.WGS84
import utilities.coordinate_systems.geographic.geocentric.spherical.GeodBasedCoordinateSystem
import utilities.common.precision.PrecisionHelper
import utilities.sampler.non_random
import scala.collection.JavaConverters._
import utilities.sampler.non_random.{ GeodeticCoordinateSample, LongitudeContainer, LatitudeContainer, GeodeticCoordinateSampler }
import utilities.coordinate_systems.geographic.geocentric.spherical.wgs84.WGS84
import scala.collection.mutable.ListBuffer
import org.apache.spark.SparkConf
import utilities.shape.{ Sphere, SingleEndedLine, Geoid }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import sim.{ StepMethod, Step, Model, Stage, Executor }
import scala.language.postfixOps
import models.light.spectral.SpectralPowerDistribution
import breeze.linalg._
import java.io.{ BufferedWriter, FileWriter }
import scala.collection.mutable.ListBuffer
case class PlanetGen(val geodeticCoordinateSystem: GeodBasedCoordinateSystem) {

  var coordinate_system = WGS84()

  var sampler = GeodeticCoordinateSampler(coordinate_system)
  var samples = sampler.generate_latitudes

  val latitude_list = samples.latitude_list.toList.filter((s: LongitudeContainer) => {
    s.latitude >= 0
  })
    
    
//latitude_list.map((l:LongitudeContainer)=>println(s"***** ${l.latitude} "))
  def vertex_list: List[Vector3] = {

    latitude_list.flatMap((a: LongitudeContainer) => {
      a.longitude_list.toList.map((v: GeodeticCoordinateSample) => {
        v.entity
      })
    })
  }

  def index_array: List[Int] = {

    //var layer_count = (samples.latitude_list.toList.length - 1) / 2 
    var layer_count = samples.latitude_list.toList.length  -1
    var res_list = ListBuffer[Int]()
    List.range(0, layer_count).map((ind: Int) => {
      if (ind == 0) {
        var layer_down = latitude_list(1).longitude_list.toList
         //println(s"Processing 90 as top layer")
         //println(s"Processing ${latitude_list(1).latitude} as next layer")
        for (v <- layer_down) {
          res_list = res_list ++ List[Int](v.index, 0)

          //println(s" 0th layer : ${v.index} and NEXT:0")   

          
        }
        res_list += 1
      } else {
        var layer = latitude_list(ind)
        var layer_down = latitude_list(ind + 1)
         //println(s"Processing ${layer.latitude} as top layer")
         //println(s"Processing ${layer_down.latitude} as next layer")
        var layer_lst = layer.longitude_list.toList
        var layer_down_lst = layer_down.longitude_list.toList
        for (v <- layer_lst) {
          var v_next = layer_lst(0)
          if (v.index < layer_lst(layer_lst.length - 1).index) {
            v_next = layer_lst(v.index-layer_lst(0).index + 1)
          }
         println(s" V : ${v.index} and V NEXT:${v_next.index}")   

          var layer_down_el: GeodeticCoordinateSample = layer_down.get_coord(v.longitude).getOrElse(null)
          var orig_layer_down_el_next: GeodeticCoordinateSample = layer_down.get_coord(v_next.longitude).getOrElse(null)
          var layer_down_el_next: GeodeticCoordinateSample = layer_down.get_coord(v_next.longitude).getOrElse(null)

          if(layer_down_el_next.index< layer_down_el.index){
            layer_down_el_next = layer_down_lst(layer_down_lst.length - 1)
          }
          println(s" down V : ${layer_down_el.index} and down V NEXT:${layer_down_el_next.index}")
          var mid = ((layer_down_el_next.index - layer_down_el.index) / 2.0).toInt

           
          //half range
          List.range(layer_down_el.index, mid + 1).map((b: Int) => {
            res_list = res_list ++ List[Int](b, v.index)

          })
          res_list += v_next.index
          res_list += mid
          List.range(mid + 1, layer_down_el_next.index + 1).map((b: Int) => {
            
            res_list = res_list ++ List[Int](b, v_next.index)

          })
          if (v_next.index == layer_lst(0).index && 
              layer_down_el_next.index != 
                layer_down_lst(layer_down_lst.length - 1).index) {
            List.range(layer_down_el_next.index, layer_down_lst(layer_down_lst.length - 1).index + 1).map((b: Int) => {
              res_list = res_list ++ List[Int](b, v_next.index)
            })
          }
             if (v_next.index == layer_lst(0).index ){
                res_list = res_list ++ List[Int](layer_down_lst(0).index, v_next.index)
             }
            
             if((orig_layer_down_el_next.index- layer_down_lst(0).index)>0){
            layer_down_el_next = layer_down_lst(layer_down_lst.length - 1)
             List.range(layer_down_lst(0).index+1, orig_layer_down_el_next.index+1).map((b: Int) => {
              res_list = res_list ++ List[Int](b, v_next.index)
            })
             }
          

          }
          res_list += layer_down_lst(0).index

        }

      

    })

    res_list.toList
  }

}