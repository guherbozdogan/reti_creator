package utilities.common.precision


case object PrecisionHelper {
  
    def format(x: AnyVal, precision: Int): String = x match {
    case x: Int    => "%d".format(x)
    case x: Long   => "%d".format(x)
    case x: Float  => s"%.${precision}f".format(x)
    case x: Double => s"%.${precision}f".format(x)
    case _         => ""
  }

  def calculate_precision(x: Double): Int = {
    var precision: Int = 0
    //this below line could have been coded better but this part is not online evaluated :P
    while ((x - (x % math.pow(10, 0 - precision))) > math.pow(10, -7)) {
      precision = precision + 1
    }
    precision
  }
  
}