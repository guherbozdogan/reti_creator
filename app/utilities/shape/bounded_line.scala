package utilities.shape
import com.github.jpbetz.subspace.{Quaternion, Vector4, Vector3}
import scala.language.postfixOps
// a line that is originally oriented at (1,0,0)
case class BoundedLine(val start:Vector4, val end:Vector4) extends Shape {
  
  
  def calc_start():Vector4 = object_to_world_matrix.*(start)
  
  def calc_end():Vector4 = object_to_world_matrix.*(end)
  
  def get_start():Vector4 = calc_start 
  
  def get_end():Vector4 = calc_end 
  
  def get_orientation():Vector4 = get_end()-get_start()
  
  def intersect(shape:Shape): List[Vector3] = {
    List[Vector3]()
  }
  
}