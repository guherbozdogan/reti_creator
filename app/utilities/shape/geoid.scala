package utilities.shape
import com.github.jpbetz.subspace.{Quaternion, Vector4, Vector3}
//import utilities.shape.{BoundedLine, UnboundedLine, SingleEndedLine}
import scala.language.postfixOps
import utilities.solver.D2_Polynomial_Solver
case class Geoid(var size:Vector3) extends Shape {
  
   def intersect(shape:Shape): List[Vector3] = {
     shape match {
       case l:BoundedLine=> {
          var tmpLine:BoundedLine = BoundedLine(l.get_start, l.get_end)
          tmpLine.copy_transformation_in_reverse_from(this)
          
          
          var X_Rad2:Double = math.pow(size.x, 2.0)
          var Y_Rad2:Double = math.pow(size.y, 2.0)
          var Z_Rad2:Double = math.pow(size.z, 2.0)
          
          
          var A:Double = math.pow(tmpLine.get_orientation.x,2.0) * Y_Rad2 * Z_Rad2 +
          math.pow(tmpLine.get_orientation.y,2.0) * X_Rad2 * Z_Rad2 +
          math.pow(tmpLine.get_orientation.z,2.0) * X_Rad2 * Y_Rad2
          
          var B:Double = (2.0*tmpLine.get_orientation.x*tmpLine.get_start.x* Y_Rad2 * Z_Rad2)+
          (2.0*tmpLine.get_orientation.y*tmpLine.get_start.y* X_Rad2 * Z_Rad2)+
          (2.0*tmpLine.get_orientation.z*tmpLine.get_start.z* X_Rad2 * Y_Rad2)
          
          var C:Double = (math.pow(tmpLine.get_start.x,2.0)* Y_Rad2 * Z_Rad2)+
          (math.pow(tmpLine.get_start.y,2.0)* X_Rad2 * Z_Rad2)+
          (math.pow(tmpLine.get_start.z,2.0)* X_Rad2 * Y_Rad2) - (X_Rad2 * Y_Rad2 * Z_Rad2)
          
          var lambda_toVector3 = (a:Any) => { a match   {
            case a:Vector4 => Vector3(a.x,  a.y, a.z)
            case (x:Double, d:Double,y:Double) => Vector3(x.toFloat, d.toFloat, y.toFloat)
            }
          }
          
          var solution_list =D2_Polynomial_Solver(A,B,C)
          //missing length control, add later
          solution_list match {
            case Nil => Nil
            case a::Nil => {
              if(a>0){ 
                  List(lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*a.toFloat))
                         
              }else {
                Nil
              }
            }
            case a::(b::Nil) =>  { 
              if ( b<0 ) {
              List( 
                  lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*a.toFloat))
               } else if (a<0) {
                
                List(lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*b.toFloat))
              } else if (a*b > 0){
                
                  List(lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*a.toFloat),
                      lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*b.toFloat))
              }
                else {
                Nil
              }
            }
            case _  => Nil
            
          }
       }
       case l:UnboundedLine=> {
          var tmpLine:UnboundedLine = UnboundedLine(l.get_start, l.get_orientation)
          tmpLine.copy_transformation_in_reverse_from(this)
          
          
          var X_Rad2:Double = math.pow(size.x, 2.0)
          var Y_Rad2:Double = math.pow(size.y, 2.0)
          var Z_Rad2:Double = math.pow(size.z, 2.0)
          
          
          var A:Double = math.pow(tmpLine.get_orientation.x,2.0) * Y_Rad2 * Z_Rad2 +
          math.pow(tmpLine.get_orientation.y,2.0) * X_Rad2 * Z_Rad2 +
          math.pow(tmpLine.get_orientation.z,2.0) * X_Rad2 * Y_Rad2
          
          var B:Double = (2.0*tmpLine.get_orientation.x*tmpLine.get_start.x* Y_Rad2 * Z_Rad2)+
          (2.0*tmpLine.get_orientation.y*tmpLine.get_start.y* X_Rad2 * Z_Rad2)+
          (2.0*tmpLine.get_orientation.z*tmpLine.get_start.z* X_Rad2 * Y_Rad2)
          
          var C:Double = (math.pow(tmpLine.get_start.x,2.0)* Y_Rad2 * Z_Rad2)+
          (math.pow(tmpLine.get_start.y,2.0)* X_Rad2 * Z_Rad2)+
          (math.pow(tmpLine.get_start.z,2.0)* X_Rad2 * Y_Rad2) - (X_Rad2 * Y_Rad2 * Z_Rad2)
          
          var solution_list =D2_Polynomial_Solver(A,B,C)
         var lambda_toVector3 = (a:Any) => { a match   {
            case a:Vector4 => Vector3(a.x,  a.y, a.z)
            case (x:Double, d:Double,y:Double) => Vector3(x.toFloat, d.toFloat, y.toFloat)
            }
          }
          
          
          
          solution_list match {
            case Nil => Nil
            case a::Nil => {
              List(lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*a.toFloat))
              }
            case a::(b::Nil) =>  { 
             List(lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*a.toFloat),
                        lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*b.toFloat))
            }
            case _  => Nil
          }
       }
       case l:SingleEndedLine=> {
          var tmpLine:SingleEndedLine = SingleEndedLine(l.get_start, l.get_orientation)
          tmpLine.copy_transformation_in_reverse_from(this)
          
          
          var X_Rad2:Double = math.pow(size.x, 2.0)
          var Y_Rad2:Double = math.pow(size.y, 2.0)
          var Z_Rad2:Double = math.pow(size.z, 2.0)
          
          
          var A:Double = math.pow(tmpLine.get_orientation.x,2.0) * Y_Rad2 * Z_Rad2 +
          math.pow(tmpLine.get_orientation.y,2.0) * X_Rad2 * Z_Rad2 +
          math.pow(tmpLine.get_orientation.z,2.0) * X_Rad2 * Y_Rad2
          
          var B:Double = (2.0*tmpLine.get_orientation.x*tmpLine.get_start.x* Y_Rad2 * Z_Rad2)+
          (2.0*tmpLine.get_orientation.y*tmpLine.get_start.y* X_Rad2 * Z_Rad2)+
          (2.0*tmpLine.get_orientation.z*tmpLine.get_start.z* X_Rad2 * Y_Rad2)
          
          var C:Double = (math.pow(tmpLine.get_start.x,2.0)* Y_Rad2 * Z_Rad2)+
          (math.pow(tmpLine.get_start.y,2.0)* X_Rad2 * Z_Rad2)+
          (math.pow(tmpLine.get_start.z,2.0)* X_Rad2 * Y_Rad2) - (X_Rad2 * Y_Rad2 * Z_Rad2)
          
          var solution_list =D2_Polynomial_Solver(A,B,C)
          var lambda_toVector3 = (a:Any) => { a match   {
            case a:Vector4 => Vector3(a.x,  a.y, a.z)
            case (x:Double, d:Double,y:Double) => Vector3(x.toFloat, d.toFloat, y.toFloat)
            }
          }
          
          
          solution_list match {
            case Nil => Nil
            case a::Nil => {
              if(a>0){ 
                List(lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*a.toFloat))
                
              }else {
                Nil
              }
            }
            case a::(b::Nil) =>  { 
              if ( a>0 ) {
                if(b>0){
              List(lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*a.toFloat),
                  lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*b.toFloat)
              )    
                }else {
              List(lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*a.toFloat))    
                }
                
              
              } else if (b>0) {
                
                List(lambda_toVector3(tmpLine.get_start) + lambda_toVector3( tmpLine.get_orientation*b.toFloat))
              } else  {
                  Nil
              }
              
            }
            case _  => Nil
            
          }
         
       
       }
     }
   }
}
          
          
          