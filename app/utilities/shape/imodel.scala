package utilities.shape
import com.github.jpbetz.subspace.{Quaternion, Vector4, Vector3,Matrix4x4}
import scala.collection.mutable.ListBuffer


case class Rotation(val rotator:Vector3, val angle:Float)
case class Translation(val v:Vector3)
case class Scale(val v:Vector3)



trait Shape {
  
  def intersect(other:Shape): List[Vector3] 
  var object_to_world_matrix:Matrix4x4 = Matrix4x4.identity
  var world_to_object_matrix:Matrix4x4 = Matrix4x4.identity
  
  var rotation_history:List[Rotation] = Nil
  var translation_history:List[Translation] = Nil
  var scale_history:List[Scale] = Nil
  
  def update():Unit = {
    
      object_to_world_matrix = Matrix4x4.identity
      world_to_object_matrix = Matrix4x4.identity

    for( rotation <-rotation_history)  {
      object_to_world_matrix = Matrix4x4.forRotation(Quaternion.forAxisAngle(rotation.rotator, rotation.angle)) * object_to_world_matrix
      world_to_object_matrix = Matrix4x4.forRotation(Quaternion.forAxisAngle(rotation.rotator, -1*rotation.angle)) * world_to_object_matrix
    
    }
    for( scale_act <-scale_history)  {
      object_to_world_matrix = Matrix4x4.forScale(scale_act.v) * object_to_world_matrix
      world_to_object_matrix = Matrix4x4.forScale(Vector3(1.0f/scale_act.v.x, 1.0f/scale_act.v.y, 1.0f/scale_act.v.z)) * world_to_object_matrix
    
    }
    for( translation_act <-translation_history)  {
      object_to_world_matrix = Matrix4x4.forTranslation(translation_act.v) * object_to_world_matrix
      world_to_object_matrix = Matrix4x4.forTranslation(translation_act.v.negate) * world_to_object_matrix
    
    }
   }
    
  def rotate(rotator:Vector3, angle:Double):Unit = {
    rotation_history = rotation_history :+ Rotation(rotator, angle.toFloat)
  }
  
  def translate(pos:Vector3):Unit = {
    
    translation_history = translation_history :+ Translation(pos)
  }
  
  //err when is scaled to 0 and exception would happen
  def scale(pos:Vector3):Unit = {
    scale_history = scale_history :+ Scale(pos)

  }
  
  def clear_transformations():Unit = {
     object_to_world_matrix = Matrix4x4.identity
     world_to_object_matrix = Matrix4x4.identity
    
     rotation_history= Nil
     translation_history = Nil
     scale_history = Nil
    
  }
  
  def copy_transformation_from(other_object: Shape) : Unit = {
    for( rotation <-other_object.rotation_history)  {
      rotate(rotation.rotator, rotation.angle)
    }
    for( scale_act <-other_object.scale_history)  {
      scale(scale_act.v)
    }
    for( translation_act <-other_object.translation_history)  {
      translate(translation_act.v)
    }
    
  }
  
  def copy_transformation_in_reverse_from(other_object: Shape) : Unit = {
    for( rotation <-other_object.rotation_history.reverse)  {
      rotate(rotation.rotator, -1* rotation.angle)
    }
    for( scale_act <-other_object.scale_history)  {
      scale(Vector3(1.0f/scale_act.v.x, 1.0f/scale_act.v.y, 1.0f/scale_act.v.z))
    }
    for( translation_act <-other_object.translation_history)  {
      translate(translation_act.v.negate)
    }
    
  }

  
  
  
  
}