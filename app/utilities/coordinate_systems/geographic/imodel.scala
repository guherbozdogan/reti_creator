package utilities.coordinate_systems.geographic

trait Location {
  
}
case object NonSupportedLocation  extends Location 

trait CoordinateSystem {
  def convert(loc:Location, targetCoordinateSystem:CoordinateSystem): Location
  
}