package utilities.coordinate_systems.geographic.geocentric.spherical

trait Latitude {
} 
trait Longitude{
  
}

case class East(val angle: Double) extends Longitude
case class West(val angle: Double) extends Longitude
case class South(val angle: Double) extends Latitude
case class North(val angle: Double) extends Latitude

  
trait SphericalCoordinateSystem {
  def r:Double
  
}

trait GeodBasedCoordinateSystem  {
  
  def flattening: Double
  
  def semi_major_axis: Double
  
  def semi_minor_axis: Double = semi_major_axis * (1.0 - 1.0/flattening)
  
  
}