package utilities.coordinate_systems.geographic.geocentric.spherical.wgs84
import utilities.coordinate_systems.geographic.{Location, NonSupportedLocation, CoordinateSystem}
import utilities.coordinate_systems.geographic.geocentric.spherical.{GeodBasedCoordinateSystem,Latitude,Longitude}
  



case class WGS84Location(val latitude:Latitude, val longitude:Longitude) extends Location

case class WGS84(val flattening:Double= 298.257223563, val semi_major_axis: Double=6378.137) extends CoordinateSystem with GeodBasedCoordinateSystem {
  
  def convert(loc:Location, targetCoordinateSystem:CoordinateSystem): Location = {
    NonSupportedLocation
  }
  
  
}