package utilities.coordinate_systems.geographic.geocentric.equirectangular
import utilities.coordinate_systems.geographic.geocentric.spherical.wgs84.{WGS84Location, WGS84}
import utilities.coordinate_systems.geographic.geocentric.spherical.{SphericalCoordinateSystem,Latitude, Longitude, East, West, South, North} 
import utilities.coordinate_systems.geographic.{Location, NonSupportedLocation, CoordinateSystem}
case class EquirectangularLocation(val x:Double, val y:Double) extends Location

//width and height are image's width and height
case class Equirectangular(val width:Double, val height:Double) extends CoordinateSystem { 
  
  def convert(loc:Location, targetCoordinateSystem:CoordinateSystem): Location = {
    loc  match {
      case locInLocal: EquirectangularLocation => {
        targetCoordinateSystem match {
        case v:WGS84 => {
          //initially 
          
          val lat: Double = (( height/2.0 - locInLocal.y )/height)*90.0
          val longt: Double = (( width/2.0 - locInLocal.x )/width)*180.0
          
          WGS84Location( { if(lat>0) {
            North(lat)} else {
             South(lat)}
          },{ if(longt>0) {
            West(longt)} else {
             East(longt)}
          }) 
          }
        //add exception instead
        case _ => NonSupportedLocation 
            
        }
      }
      //add exception instead
      case _ => NonSupportedLocation
    }
    
    }
    
  }
 