package utilities.solver

object D2_Polynomial_Solver {
  def apply(a:Double, b:Double, c:Double):List[Double] = {
    var B2:Double = math.pow(b, 2.0)
    var AC4 : Double =4.0*a*c
    if (B2 < AC4) {
      return Nil
    } else  if(B2==AC4)  {
      List(-1.0*b /(2.0*a)) 
    } else {
        var tmp:Double=math.sqrt(B2-AC4)
        var res =((tmp - b)/(2.0*a),(-1.0*tmp - b)/(2.0*a))
        if(res._1 < res._2){
        List(res._1, res._2)
        }else{
          List(res._2, res._1)
        }
    }
  }
}