package models.orbit_model.kepler
import org.joda.time.DateTime

  
   
case class EarthsOrbit(val  mass_of_sun:Double = 1.989 * math.pow(10,30), 
    val eccentricity_of_orbit:Double= 0.0167, 
    val semi_major_axis_len:Double = 149.60 * math.pow(10, 6),
    val day_of_perihelion:DateTime =  new DateTime(2018,1,3,0,0),
    val day_of_equinox:DateTime =  new DateTime(2018,3,20,0,0),
    val tilt_angle_from_z_clockwise:Double = 23.44 
    )
        extends KeplerModel;





  
 