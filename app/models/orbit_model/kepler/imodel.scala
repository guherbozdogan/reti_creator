package models.orbit_model.kepler

import scala.language.postfixOps
import org.joda.time.{DateTime, Period, PeriodType}
import com.github.jpbetz.subspace.{Quaternion, Vector4, Vector3}


case class PolarLocation (val R:Double, val theta:Double)


trait KeplerModel{
   var G:Double =  6.67384 * math.pow(10.0, -20.0)
    
   def tilt_angle_from_z_clockwise: Double
   
   def day_of_perihelion : DateTime
   
   def day_of_equinox: DateTime
   
   def mass_of_sun: Double 
    
   def eccentricity_of_orbit: Double  
   
   def semi_major_axis_len: Double 
   
   def L: Double  =  semi_major_axis_len * (1.0 - math.pow( eccentricity_of_orbit, 2.0))

   def K: Double =  G * mass_of_sun  
   
   def H: Double  =  math.sqrt(L * K)
   
   def A:Double  =  semi_major_axis_len
   
   def B:Double =  L / (math.sqrt(1.0- eccentricity_of_orbit))  
   
   def C:Double = eccentricity_of_orbit * A 
   
   def R__for_0_theta:Double = A - C
   
   def calc_R_of_theta(theta:Double):Double = {
        L * (1.0+ eccentricity_of_orbit*math.cos(theta))
   }
   
   def polar_loc_for_first_equinox:PolarLocation = {
     val period = new Period(day_of_perihelion, day_of_equinox, PeriodType.seconds())
     non_accurately_move_theta_for_n_seconds(PolarLocation( R__for_0_theta,0.0.toFloat), period.getSeconds)
   }
   
   //the vector which is perpendicular to tilt's plane
   def tilt_rotator_vector: Vector3 = {
        val loc = polar_loc_for_first_equinox
        Quaternion.forAxisAngle(Vector3(0,0,1),loc.theta.toFloat ).*( Vector3(1,0,0))
   }
   
   def tilt_quaternion:Quaternion = {
          Quaternion.forAxisAngle(tilt_rotator_vector, tilt_angle_from_z_clockwise.toFloat)
      }
  
   def tilt_orientation:Vector3 = {
     
     tilt_quaternion.*(Vector3(0,0,1))
   }
   
   def non_accurately_move_theta_for_n_seconds(loc: PolarLocation , n:Int):PolarLocation ={
       var theta_v=loc.theta
       var R_v =loc.R
       for  (i <- 1 to n) {
            theta_v = theta_v + (H / math.pow(R_v,2) )
            R_v = calc_R_of_theta(theta_v)
       }

        PolarLocation(R_v, theta_v)
    }
   
}