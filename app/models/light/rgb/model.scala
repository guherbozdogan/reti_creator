package models.light.color

import models.light.{WaveLength, Color,Flux_of_Wavelength}


case class RGBColor(val R:Int, val G:Int, val B:Int, val Alphae:Double) extends Color

case object RGBColor {
  //this function is taken from gist of 
  //https://gist.github.com/friendly/67a7df339aa999e2bcfcfec88311abfc
  def convert(wavelength: Double, gamma:Double=0.8): RGBColor = {
      
    var R,G,B = 0.0
    if (wavelength >= 380.0 & wavelength <= 440.0) {
        var attenuation = 0.3 + 0.7 * (wavelength - 380.0) / (440.0 - 380.0)
        R = math.pow(((-(wavelength - 440.0) / (440.0 - 380.0)) * attenuation),gamma)
        G = 0.0
        B = math.pow((1.0 * attenuation) , gamma)
        }
    else if (wavelength >= 440.0 & wavelength <= 490.0) {
        R = 0.0
        G = math.pow(((wavelength - 440.0) / (490.0 - 440.0)) , gamma)
        B = 1.0
        }
    else if (wavelength >= 490.0 & wavelength <= 510.0) {
        R = 0.0
        G = 1.0
        B = math.pow((-(wavelength - 510.0) / (510.0 - 490.0)) , gamma)
        }
    else if (wavelength >= 510.0 & wavelength <= 580.0) {
        R = math.pow(((wavelength - 510.0) / (580.0 - 510.0)) , gamma)
        G = 1.0
        B = 0.0
        }
    else if (wavelength >= 580.0 & wavelength <= 645.0) {
        R = 1.0
        G = math.pow((-(wavelength - 645.0) / (645.0 - 580.0)) , gamma)
        B = 0.0
        }
    else if (wavelength >= 645.0 & wavelength <= 750.0) {
        var attenuation = 0.3 + 0.7 * (750.0 - wavelength) / (750.0 - 645.0)
        R = math.pow((1.0 * attenuation) , gamma)
        G = 0.0
        B = 0.0
        }
    else {
        R = 0.0
        G = 0.0
        B = 0.0
        }
   if(R>1.0)
     R=1.0
     if(G>1.0)
     G=1.0
     if(B>1.0)
     B=1.0
    R = R * 255.0
    G = G * 255.0
    B = B * 255.0
    return RGBColor(math.floor(R).toInt, math.floor(G).toInt, math.floor(B).toInt, 1.0)
  }

    def build(l:List[Flux_of_Wavelength]) :RGBColor = {
      
      var intensity_sum = l.foldLeft(0.0)((sum:Double, c:Flux_of_Wavelength) => {
        
       sum+(c.intensity/80000.0)
      })
      if(intensity_sum == 0.0){
        intensity_sum=0.00000000000001
      }
      
      
//      var weighted_wavelength= l.foldLeft(0.0)((sum:Double, c:Flux_of_Wavelength) => {
//      sum  + c.wavelength*(c.intensity/intensity_sum)
//      })
      
      l.map((c:Flux_of_Wavelength)=>{
        
        var tmp_gamma_adjustment=c.intensity/80000
        if (tmp_gamma_adjustment == 0.0)
          tmp_gamma_adjustment = 0.0000000000001
        (convert(c.wavelength.v), c.intensity)   
        
      }).foldLeft((0.0,0.0,0.0))((ac:Tuple3[Double,Double,Double] , c:Tuple2[RGBColor, Double]) => {
          (ac._1+((c._1.R*c._2/80000.0)/intensity_sum),ac._2+((c._1.G*c._2/80000.0)/intensity_sum)
              ,ac._3+((c._1.B*c._2/80000.0)/intensity_sum))
      }) match { case c:Tuple3[Double,Double,Double]=>{
         RGBColor(math.floor(c._1).toInt,math.floor(c._2).toInt,math.floor(c._3).toInt, 1.0)
      }}
    }
 }