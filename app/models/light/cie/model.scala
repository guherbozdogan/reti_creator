package models.light.cie
import models.light.{ WaveLength, Color, Flux_of_Wavelength }
import breeze.linalg._
import models.light.color.RGBColor

import breeze.linalg._
import java.io._
import breeze.linalg.{convert, max, min}
//all logic taken from scipy library and page of: https://scipython.com/blog/converting-a-spectrum-to-a-colour/

case class CIEColor(val x: Double, val y: Double, val Y: Double) extends Color

trait RGB_Conv_Preset {
  def red: DenseMatrix[Double]
  def green: DenseMatrix[Double]
  def blue: DenseMatrix[Double]
  def white: DenseMatrix[Double]

}

case class SRGB_Preset(
  val red:   DenseMatrix[Double] = CIE.xyz_from_xy(0.64, 0.33),
  val green: DenseMatrix[Double] = CIE.xyz_from_xy(0.30, 0.60),
  val blue:  DenseMatrix[Double] = CIE.xyz_from_xy(0.15, 0.06),
  val white: DenseMatrix[Double] = CIE.xyz_from_xy(0.3127, 0.3291)) extends RGB_Conv_Preset
case class hdtv_Preset(
  val red:   DenseMatrix[Double] = CIE.xyz_from_xy(0.67, 0.33),
  val green: DenseMatrix[Double] = CIE.xyz_from_xy(0.21, 0.71),
  val blue:  DenseMatrix[Double] = CIE.xyz_from_xy(0.15, 0.06),
  val white: DenseMatrix[Double] = CIE.xyz_from_xy(0.3127, 0.3291)) extends RGB_Conv_Preset

case object CIE {
  def xyz_from_xy(x: Double, y: Double): DenseMatrix[Double] = {
    return DenseMatrix((x, y, 1 - x - y))
  }

}
case object CIE_SRGB { 
  
  var cie =CIE( hdtv_Preset())
}
case class CIE(val rgb_preset: RGB_Conv_Preset) {

  val cmf = csvread(new File("data/models/cie/cie-cmf.txt"), ' ').delete(0, Axis._1)
  
  val M = DenseMatrix.vertcat(rgb_preset.red, rgb_preset.green, rgb_preset.blue).t
  val MI = inv(M)
  val wscale = (MI * rgb_preset.white.t).t
  val T = MI(::, *) / wscale.toDenseVector
  
  
  
  
  def xyz_to_rgb(xyz:DenseVector[Double], out_fmt:String=null):Any =  {
        var rgb = (T * xyz.toDenseMatrix.t).t
        if (any(rgb.map(v=> v< 0.0))){
            var w = - min(rgb)
            rgb = rgb + w
        }
        if(all(rgb.map(v=>v==0)) == false) {
            rgb = rgb / max(rgb)
        }

        var rgb_i:DenseMatrix[Int] =(rgb*255.0).map(a=>a.toInt)    
        if (out_fmt == "html")  {
            rgb_to_hex(rgb_i)
        }else {
          RGBColor(rgb_i(0,0),rgb_i(0,1),rgb_i(0,2),0.0)
        }
  }  

    def rgb_to_hex(rgb_i:DenseMatrix[Int]):String = { 
  
        f"${rgb_i(0,0)}%X${rgb_i(0,1)}%X${rgb_i(0,2)}%X"

     }

    def spec_to_xyz(spec:DenseVector[Double]):DenseVector[Double] = { 
          
      var summation = cmf.t * diag(spec)
         
        var  XYZ = sum(summation(*, ::))

        var den = sum(XYZ)
        if (den == 0.0) {
            XYZ
        } else {
          XYZ / den
        }
        
        
    }
        
    def spec_to_rgb(spec:DenseVector[Double], out_fmt:String=null):Any = {
      
        var xyz = spec_to_xyz(spec)
        xyz_to_rgb(xyz, out_fmt)
    }


}