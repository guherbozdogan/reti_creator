package models.light.spectral
import breeze.linalg._
import models.light.color.RGBColor

import breeze.linalg._
import java.io._
import breeze.linalg.{convert, max, min}
//all logic taken from scipy library 
case object SpectralPowerDistribution {
  
  val h = 6.62607004 * math.pow(10.0, -34)
  val c = 299792458.0
  val k =  1.38064852* math.pow(10.0,-23)
//  
//      lam_m = lam / 1.e9
//    fac = h*c/lam_m/k/T
//    B = 2*h*c**2/lam_m**5 / (np.exp(fac) - 1)
//    return B
  def planck(lam : DenseVector[Double], T:Double):DenseVector[Double] ={
    
  
//    """ Returns the spectral radiance of a black body at temperature T.
//
//    Returns the spectral radiance, B(lam, T), in W.sr-1.m-2 of a black body
//    at temperature T (in K) at a wavelength lam (in nm), using Planck's law.
//
//    """

    var lam_m = lam / math.pow(10.0,9.0)
    var fac = lam_m.map(v=> h*c/v/k/T)
    var tmp_lam_m=   lam_m.map(a=>math.pow(a,5.0))
    var inner_calc = tmp_lam_m.map( v=> 2*h*math.pow(c,2)/ v)
    
    
    inner_calc /:/  fac.map(f=>{ math.exp(f)-1.0})
    
    
  
}
}