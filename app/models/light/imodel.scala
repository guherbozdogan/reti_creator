package models.light
 


trait Color {
  
}
case class WaveLength(val v:Double)

case class Flux_of_Wavelength(wavelength: WaveLength, intensity:Double) 

case object Flux {
  
  def normalize_SPD (list:List[Flux_of_Wavelength]):Array[Double] ={
    
    var sum = list.foldLeft(0.0)((a:Double, v:Flux_of_Wavelength)=>{
      a+v.intensity
    })
    
    list.map((a:Flux_of_Wavelength)=>{
      
          a.intensity/sum 
    }).toArray
    
  }
}
 