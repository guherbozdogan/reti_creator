package models.atmosphere.earth
import models.atmosphere.{AtmosphereContent,AtmosphereLayer, Atmosphere }
//its not very physical realistic since i am not any expert in these topics:D

 
case class TroposphereAreosolOfWater( val concentration:Double, val x:Double,val isCloud:Boolean=false,val is_continental: Boolean=true, val is_urban:Boolean=false) extends   AtmosphereContent {
  def optical_depth(wavelength:Double):Double =  {
       wavelength match {
         case _  =>  {
           if (is_continental){
             0.34
           }else if(is_urban) {
             0.84
           }else {
               0.84
           }
         }
       }
  }
     override def extinction_multiplier(wavelength:Double, length:Double): Double = {
    var res = math.exp(-1.0* optical_depth(wavelength)*length)
   // print("Optical depth: " )
    //println(optical_depth(wavelength))
    //print("Extinction: ")
    //println(res)
    res + (1-res)/2.0
  }
}
  
case class TroposphereAreosolOfDust( val concentration:Double, val x:Double,val isCloud:Boolean=false,val is_continental: Boolean=true, val is_urban:Boolean=false) extends   AtmosphereContent {
  def optical_depth(wavelength:Double):Double =  {
    
       wavelength match {
         case _  =>  {
           if (is_continental){
             0.00079
           }else if(is_urban) {
             0.00035
           }else {
               0.00035
           }
         }
       }
  }
}
case class TroposphereAreosolOfSoot( val concentration:Double, val x:Double,val isCloud:Boolean=false,val is_continental: Boolean=true, val is_urban:Boolean=false) extends   AtmosphereContent {
  def optical_depth(wavelength:Double):Double =  {
    
       wavelength match {
         case _  =>  {
           if (is_continental){
             0.0014
           }else if(is_urban) {
             0.0033
           }else {
               0.0033
           }
         }
       }
  }
}

case class StratosphereAreosolOfSulfate( val concentration:Double, val x:Double,val isCloud:Boolean=false,val is_continental: Boolean=true, val is_urban:Boolean=false) extends   AtmosphereContent {
  def optical_depth(wavelength:Double):Double =  {
    
       wavelength match {
         case _  =>  {
           if (is_continental){
             0.00026
           }else if(is_urban) {
             0.00026
           }else {
               0.00026
           }
         }
       }
  }
}

case class MoleculerContent( val concentration:Double, val x:Double,val isCloud:Boolean=false,val is_continental: Boolean=true, val is_urban:Boolean=false)  extends AtmosphereContent { 
  def optical_depth(wavelength:Double):Double =  {
       (wavelength - wavelength%50) match {
         case 300 => 1.2237
         case 350 => 0.5653  
         case 400 => 0.3641
         case 450 => 0.3641
         case 500 => 0.1452
         case 550 => 0.0984
         case 600 => 0.0690
         case 650 => 0.0690
         case 700 => 0.0369
         case 750 => 0.0369
         case 800 => 0.0215
         case 850 => 0.0215
         case 900 => 0.0134
         case 950 => 0.0134
         case 1000 => 0.0072
       } 
       
    }
  
   override def extinction_multiplier(wavelength:Double, length:Double): Double = {
    var res = math.exp(-1.0* optical_depth(wavelength)*length)
   // print("Optical depth: " )
    //println(optical_depth(wavelength))
    //print("Extinction: ")
    //println(res)
    res + (1-res)/2
  }
    
}


case  class EarthAtmosphere() extends Atmosphere {
  def build : Unit = {
    val earth_rad = 6378.137
    //later on detail this currently a very simple model
    List.range(1,10) map ((a:Int)=>{
      if (a<=6) {
        var atmosphereLayer = AtmosphereLayer(earth_rad +a)
        atmosphereLayer= atmosphereLayer.addContentTo(atmosphereLayer,MoleculerContent(0.0, 0.0, false, true, false))
        .addContentTo(atmosphereLayer, TroposphereAreosolOfWater(0.0, 0.0, false, true, false))
        this.addLayerTo(atmosphereLayer)
        //Atmosphere.addLayerTo(this, atmosphereLayer)
        //lets check urban things on this
        
      }else if (a<12) {
        var atmosphereLayer = AtmosphereLayer(earth_rad +a)
        atmosphereLayer= atmosphereLayer.addContentTo(atmosphereLayer,MoleculerContent(0.0, 0.0, false, true, false))
        .addContentTo(atmosphereLayer, TroposphereAreosolOfWater(0.0, 0.0, false, true, false))
        this.addLayerTo(atmosphereLayer)
      }
        else if (a>=12 && a<=26){
           var atmosphereLayer = AtmosphereLayer(earth_rad +a)
           
         atmosphereLayer= atmosphereLayer.addContentTo(atmosphereLayer, StratosphereAreosolOfSulfate(0.0, 0.0, false, true, false))
        this.addLayerTo(atmosphereLayer)
           
      }
        else {
           var atmosphereLayer = AtmosphereLayer(earth_rad +a)
        this.addLayerTo(atmosphereLayer)
      }
    })
    
    
    
  }
}

