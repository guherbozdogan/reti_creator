package models.atmosphere
//its not very physical realistic since i am not any expert in these topics:D

//my project-> so i am the boss :D  
trait AtmosphereContent {
  //wavelength is in nano meters:)
  def optical_depth(wavelength:Double):Double
  def extinction_multiplier(wavelength:Double, length:Double): Double = {
    var res = math.exp(-1.0* optical_depth(wavelength)*length)
   // print("Optical depth: " )
    //println(optical_depth(wavelength))
    //print("Extinction: ")
    //println(res)
    res
  }
}

case class AtmosphereLayer(val radius: Double) {
  var contents : List[AtmosphereContent] =List[AtmosphereContent]() 
    def addContentTo(atmosphere_layer:AtmosphereLayer, content:AtmosphereContent):AtmosphereLayer  = {
    atmosphere_layer.contents =atmosphere_layer.contents  ++ List[ AtmosphereContent] (content)
    atmosphere_layer
  }
}

trait Atmosphere {
  var layers: List[AtmosphereLayer] =  List[AtmosphereLayer]()
   def addLayerTo(layer:AtmosphereLayer): Atmosphere = {
    layers =layers  ++ List[ AtmosphereLayer] (layer)
    this
  }  
}
  
