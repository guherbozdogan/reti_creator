package models.planet.earth

import models.planet.Planet
//import squants.mass.{Grams, Kilograms}
//import squants.space.{Volume}
//import scala.language.postfixOps
//import squants.energy.EnergyConversions._
//import squants.energy.PowerConversions._
//import squants.information.InformationConversions._
//import squants.market.MoneyConversions._
//import squants.space.LengthConversions._
//import squants.time.TimeConversions._
//import squants.thermal._
////import squants._
//import scala.math.{log,exp}
//import squants.motion.Velocity
//import squants.time.{ Seconds, _ }
//import squants.space.{Degrees}
//import squants.time.{Hours, Days}
//import squants.mass.{Kilograms, Pounds}
//import squants.energy.Energy
//import squants.radio.{ WattsPerSquareMeter }
////import models.planet.OrbitalParams
////import models.planet.Planet
//import squants.space.{Kilometers, Length, SquareKilometers, CubicMeters}
//import squants.motion.{Acceleration, Velocity,KilometersPerSecond, MetersPerSecond}
//import squants.space.{Angle,Degrees}
//
//import scala.language.postfixOps
//import squants.energy.EnergyConversions._
//import squants.energy.PowerConversions._
//import squants.information.InformationConversions._
//import squants.market.MoneyConversions._
//import squants.space.LengthConversions._
//import squants.time.TimeConversions._
//
//import squants.energy.{Kilowatts, MegawattHours, Power}
//import squants.market.{Price, USD}
//import squants.time.Seconds
//
//
//class EarthOrbitalParams extends OrbitalParams{
//    
//   val init  = {
//        semi_major_axis = Kilometers(149.60* math.pow(10, 6))  
//        sidereal_orbit_period = Days(365.256)
//        tropical_orbit_period  = Days(365.242)
//    
//        perihelion  = Kilometers(147.09  * math.pow(10,6))
//    
//        aphelion  = Kilometers(152.10 * math.pow(10,6))
//    
//        mean_orbital_velocity  = KilometersPerSecond(29.78)
//    
//        min_orbital_velocity  = KilometersPerSecond(30.29)
//
//        max_orbital_velocity  = KilometersPerSecond(29.29)
//    
//        orbit_inclination  = Degrees(0.0)
//    
//        orbit_eccentricity  = 0.0167
//    
//        sidereal_rotation_period  = Hours(23.9345)
//    
//        length_of_day  = Hours(24)
//    
//        obliquity_to_orbit  = Degrees(23.44)
//    
//        inclination_of_equator  = Degrees(23.44)
//        
//        1
//        
//    }
//    
//    
//    
//    
//}
//
//object EarthOrbitalParams {
//  
//}
//
//
//
//class Earth extends Planet {
//  
//  import squants.time.TimeConversions._ 
//  
//  val init  = {
//    mass  = Kilograms(5.9723*1024)
//  
//    volume  = CubicMeters(math.pow(10,9)*1010*108.321)
//  
//    equatorial_radius  = Kilometers(6378.137)    
//  
//     polar_radius  = Kilometers(6356.752)
//
//     ellipticity  = 0.003353  
//  
//     surface_gravity  = MetersPerSecond(9.798)/second
//  
//     escape_velocity  = MetersPerSecond(9.780)
//  
//     GM_const  = 0.39860*math.pow(10,9)
//  
//     solar_irradiance  = WattsPerSquareMeter(1361.0)
//  
//     num_satellite  = 1
//         
//     has_planetry_ring_system  = false
//   
//     black_body_temperature  = Kelvin(254.0)
//  
//    orbital_params = new EarthOrbitalParams  
//    1
//  }
//  
//  
//}
//
case class Earth() extends Planet {
  
  //import squants.time.TimeConversions._ 
  override val equatorial_radius = 6378.137
  
//  val init  = {
//    mass  = Kilograms(5.9723*1024)
//  
//    volume  = CubicMeters(math.pow(10,9)*1010*108.321)
//  
//    equatorial_radius  = Kilometers(6378.137)    
//  
//     polar_radius  = Kilometers(6356.752)
//
//     ellipticity  = 0.003353  
//  
//     surface_gravity  = MetersPerSecond(9.798)/second
//  
//     escape_velocity  = MetersPerSecond(9.780)
//  
//     GM_const  = 0.39860*math.pow(10,9)
//  
//     solar_irradiance  = WattsPerSquareMeter(1361.0)
//  
//     num_satellite  = 1
//         
//     has_planetry_ring_system  = false
//   
//     black_body_temperature  = Kelvin(254.0)
  
//    orbital_params = new EarthOrbitalParams  
   //1
  //}
  
  
}

//