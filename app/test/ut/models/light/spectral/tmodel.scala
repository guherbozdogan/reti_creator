package test.ut.models.light.spectral

import sim.{ Step, StepMethod, Executor, ExecutorWithContext }
import sim.spark.{ SparkStep, SparkExecutorWithContext }
import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.non_random.{ SphereCoordinateSample, SphereSampler }
import models.atmosphere.earth.{ EarthAtmosphere }
import models.atmosphere.{ Atmosphere, AtmosphereLayer, AtmosphereContent }
import org.apache.spark.sql.{ SparkSession, SQLContext }
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import utilities.shape.{ Sphere, SingleEndedLine }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import sim.{ StepMethod, Step, Model, Stage, Executor }
import scala.language.postfixOps
import models.light.spectral.SpectralPowerDistribution
import breeze.linalg._
import breeze.linalg._
import breeze.plot._
import models.light.cie.{CIE,CIE_SRGB}
object Test {

  def main(args: Array[String]): Unit = {
    var range = 380 to 780 by 5 toArray
    var range_d = range.map(
      (a: Int) => a.toDouble)

    var range_x = new DenseVector[Double](range_d)
    var range_dist = SpectralPowerDistribution.planck(
      new DenseVector[Double](range.map(
        (a: Int) => a.toDouble)), 5778.0)

    val f = Figure()
    val p = f.subplot(0)
    p += plot(range_x, range_dist)
    p.xlabel = "x axis"
    p.ylabel = "y axis"
    f.saveas("lines.png")
    
    println(CIE_SRGB.cie.spec_to_rgb(range_dist, "html"))
    
  }
}
 
