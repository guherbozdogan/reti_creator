package test.ut.utilities.gen.planet
import sim.{ Step, StepMethod, Executor, ExecutorWithContext }
import sim.spark.{ SparkStep, SparkExecutorWithContext }
import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.non_random.{ SphereCoordinateSample, SphereSampler }
import models.atmosphere.earth.{ EarthAtmosphere }
import models.atmosphere.{ Atmosphere, AtmosphereLayer, AtmosphereContent }
import org.apache.spark.sql.{ SparkSession, SQLContext }
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import utilities.shape.{ Sphere, SingleEndedLine, Geoid }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import sim.{ StepMethod, Step, Model, Stage, Executor }
import scala.language.postfixOps
import models.light.spectral.SpectralPowerDistribution
import breeze.linalg._
import java.io.{ BufferedWriter, FileWriter }
import scala.collection.JavaConverters._
import utilities.sampler.non_random.{GeodeticCoordinateSample, LongitudeContainer, LatitudeContainer , GeodeticCoordinateSampler}
import  utilities.coordinate_systems.geographic.geocentric.spherical.wgs84.WGS84
import scala.collection.mutable.ListBuffer

import scala.util.Random

import au.com.bytecode.opencsv.CSVWriter
import utilities.gen.planet.PlanetGen


object TestPlanetGen {

  def main(args: Array[String]): Unit = {

    var coordinate_system = WGS84()
    var planet_gen =PlanetGen(coordinate_system)
      
    var vertex_list = planet_gen.vertex_list
    var index_list =planet_gen.index_array
    
    val outputFile_vertex = new BufferedWriter(new FileWriter("test_vertex.csv")) //replace the path with the desired path and filename with the desired filename

    val csvWriter = new CSVWriter(outputFile_vertex)

    val csvFields_vertex = Array("x", "y", "z")

    var listOfRecords_vertex = new ListBuffer[Array[String]]()

    listOfRecords_vertex += csvFields_vertex

    listOfRecords_vertex ++= vertex_list.map((a: Vector3) => {
      
          Array[String](a.x.toString, a.y.toString, a.z.toString)
      })
      
      
      

    csvWriter.writeAll(listOfRecords_vertex.toList.asJava)

    outputFile_vertex.close()
    
    
    
        
    val outputFile_index = new BufferedWriter(new FileWriter("test_index.csv")) //replace the path with the desired path and filename with the desired filename

    val csvWriter_index = new CSVWriter(outputFile_index)

    val csvFields_index = Array("index")

    var listOfRecords_index = new ListBuffer[Array[String]]()

    listOfRecords_index += csvFields_index

    listOfRecords_index ++= index_list.map((a: Int) => {
      
          Array[String](a.toString)
      })
      
      
      

    csvWriter_index.writeAll(listOfRecords_index.toList.asJava)

    outputFile_index.close()

  }

}
