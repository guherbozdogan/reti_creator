package test.utilities.sampler.non_random

import sim.{ Step, StepMethod, Executor, ExecutorWithContext }
import sim.spark.{ SparkStep, SparkExecutorWithContext }
import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.non_random.{ SphereCoordinateSample, SphereSampler }
import models.atmosphere.earth.{ EarthAtmosphere }
import models.atmosphere.{ Atmosphere, AtmosphereLayer, AtmosphereContent }
import org.apache.spark.sql.{ SparkSession, SQLContext }
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import utilities.shape.{ Sphere, SingleEndedLine, Geoid }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import sim.{ StepMethod, Step, Model, Stage, Executor }
import scala.language.postfixOps
import models.light.spectral.SpectralPowerDistribution
import breeze.linalg._
import java.io.{ BufferedWriter, FileWriter }
import scala.collection.JavaConverters._
import utilities.sampler.non_random.{GeodeticCoordinateSample, LongitudeContainer, LatitudeContainer , GeodeticCoordinateSampler}
import  utilities.coordinate_systems.geographic.geocentric.spherical.wgs84.WGS84
import scala.collection.mutable.ListBuffer

import scala.util.Random

import au.com.bytecode.opencsv.CSVWriter

object TestGeodeticCoordinateSampler {

  def main(args: Array[String]): Unit = {
    var coordinate_system = WGS84()

    var sampler = GeodeticCoordinateSampler(coordinate_system)

    var samples = sampler.generate_latitudes

    val outputFile = new BufferedWriter(new FileWriter("test_geoid_sampler.csv")) //replace the path with the desired path and filename with the desired filename

    val csvWriter = new CSVWriter(outputFile)

    val csvFields = Array("x", "y", "z")

    var listOfRecords = new ListBuffer[Array[String]]()

    listOfRecords += csvFields

    listOfRecords ++= samples.latitude_map.values.toList.flatMap((s: LongitudeContainer) => {
      
      s.longitude_map.values.toList.map((a:GeodeticCoordinateSample)=>{
          Array[String](a.entity.x.toString, a.entity.y.toString, a.entity.z.toString)
      })
      
    })

    csvWriter.writeAll(listOfRecords.toList.asJava)

    outputFile.close()

  }

}
