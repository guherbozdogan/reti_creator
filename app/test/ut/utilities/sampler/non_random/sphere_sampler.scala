package test.utilities.sampler.non_random

import sim.{ Step, StepMethod, Executor, ExecutorWithContext }
import sim.spark.{ SparkStep, SparkExecutorWithContext }
import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.non_random.{ SphereCoordinateSample, SphereSampler }
import models.atmosphere.earth.{ EarthAtmosphere }
import models.atmosphere.{ Atmosphere, AtmosphereLayer, AtmosphereContent }
import org.apache.spark.sql.{ SparkSession, SQLContext }
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import utilities.shape.{ Sphere, SingleEndedLine }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import sim.{ StepMethod, Step, Model, Stage, Executor }
import scala.language.postfixOps
import models.light.spectral.SpectralPowerDistribution
import breeze.linalg._
import java.io.{ BufferedWriter, FileWriter }
import scala.collection.JavaConverters._

import scala.collection.mutable.ListBuffer

import scala.util.Random

import au.com.bytecode.opencsv.CSVWriter

object TestSphereSampler {

  def main(args: Array[String]): Unit = {
    var planet = Earth()

    var sphere_sampler = SphereSampler(planet.equatorial_radius)

    var samples = sphere_sampler.generate_samples

    val outputFile = new BufferedWriter(new FileWriter("test_sphere_sampler.csv")) //replace the path with the desired path and filename with the desired filename

    val csvWriter = new CSVWriter(outputFile)

    val csvFields = Array("x", "y", "z")

    var listOfRecords = new ListBuffer[Array[String]]()

    listOfRecords += csvFields

    listOfRecords ++= samples.map((s: SphereCoordinateSample) => {
      Array[String](s.entity.x.toString, s.entity.y.toString, s.entity.z.toString)
    })

    csvWriter.writeAll(listOfRecords.toList.asJava)

    outputFile.close()

  }

}
