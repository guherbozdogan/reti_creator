package test.ut.utilities.shape

import sim.{ Step, StepMethod, Executor, ExecutorWithContext }
import sim.spark.{ SparkStep, SparkExecutorWithContext }
import com.github.jpbetz.subspace.{ Quaternion, Vector4, Vector3 }
import utilities.sampler.non_random.{ SphereCoordinateSample, SphereSampler }
import models.atmosphere.earth.{ EarthAtmosphere }
import models.atmosphere.{ Atmosphere, AtmosphereLayer, AtmosphereContent }
import org.apache.spark.sql.{ SparkSession, SQLContext }
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import utilities.shape.{ Sphere, SingleEndedLine }
import models.planet.Planet
import models.planet.earth.Earth
import models.light.{ WaveLength, Flux_of_Wavelength }
import sim.{ StepMethod, Step, Model, Stage, Executor }
import scala.language.postfixOps
import scala.util.Random

import au.com.bytecode.opencsv.CSVWriter
//worst test case ever written  :P 
// not any holdng any correctly software writing practices for ut   
//fix later on this lack of software quality 
object TestSphere {

  def main(args: Array[String]): Unit= {
    
    var check_if_obscured_by_planet = (c: Vector3,
      sun_light: Vector3, planet: Planet) => {
      var rev_light = SingleEndedLine(
        Vector4(c.x, c.y, c.z, 1),
        Vector4(sun_light.x * (-1), sun_light.y * (-1), sun_light.z * (-1), 1))

      var sphere = Sphere(planet.equatorial_radius)
      if (sphere.intersect(rev_light).length > 0) {
        true
      } else {
        false
      }
    }
    
        var planet = Earth()
        
         var sun_light_dir = Vector3(1, 0, 0)
         
         println(check_if_obscured_by_planet(Vector3(4649.864.toFloat, 
             4079.025.toFloat , 1588.416.toFloat), sun_light_dir , planet  ))
    
  }
}